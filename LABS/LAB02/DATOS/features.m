function features(file, w, d)
% features (file, w, d)
% file = fichero con la senal
% w = tamano de la ventana (200)
% d = desplazamiento entre ventanas (w/2)

if nargin < 2
    w = 200;
end
if nargin < 3
    d=w/2;
end

fragL = w;
desp = d;

[x, fs] = audioread(file);

Ex = [];        % Energia
zcr = [];       % Zero-Crossing Rate

ifrag = 1:fragL;
while (ifrag(end) <= length(x))
    Ex = [Ex, sum(x(ifrag).^2)];
    zcr = [zcr, sum(sign(x(ifrag)) ~= sign(x(ifrag+1)))];
    
    ifrag = ifrag + desp;
end

tx = (0:length(x)-1)/fs;
tfrags = (fragL/2 + (0:length(Ex)-1)*desp)/fs;
figure
subplot(3,1,1)
    plot(tx, x)
    grid
    title('x(n)')
subplot(3,1,2)
    plot(tfrags, Ex)
    grid
    title('Energia')
subplot(3,1,3)
    plot(tfrags, zcr)
    grid
    title('Zero-Crossing Rate')
