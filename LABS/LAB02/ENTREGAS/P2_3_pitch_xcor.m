Nmin = 11;			% Rango de periodos posibles
Nmax = 88;
WIDTH = 200;		% Nº de puntos de cada fragmento

[melo, fs] = audioread('LABS/LAB02/DATOS/melodia.wav');			% carga de la señal de sonido
nfrag = fix( length(melo)/WIDTH );		% calculo de nº de fragmentos

% Obtención de las frecuencias fundamentales en el vector f0 de nfrag
% elementos

%mirar el features.m


d = Nmax * 2;
ifrag = 1:WIDTH;

f0 = [];
m0 = [];
m1 = [];

% 
% rxx = xcorr(melo(1:400));
% figure;
% plot(rxx(400:799));
% 
% % 400 + Tmin -1 : 
% secondMax = max(rxx(400 + 10:799))
% % OBTENER EL SIGUIENTE VALOR MÁXIMO DESPUES DEL ZEROCROSSING
% 
% 
% figure;
% plot(rxx);



while (ifrag(end) <= length(melo))

    
    rxx = xcorr(melo(ifrag));
    
    ifrag = ifrag + d;
    
    % recoger los valores y meterlos en f0

    % TODO revisar los rangos
    secondMax = max(rxx(ifrag + 10:ifrag+80))
    
    f0 = [f0, secondMax];


end


% ir moviendo la venta y recogiendo los valores

figure;
plot(m0 - m1);




% Dibujo del resultado
figure
plot(f0) %supone que he usado el f0 para almacenar las frecuencias
drawnow

% Generación de la melodía correspondiente
fase_ini=0;;
mus=[];
for k=1:nfrag ;
   fase=2*pi*f0(k)*(1:WIDTH)/11025 + fase_ini;
   mus=[mus sin(fase)];
   fase_ini=fase(end);
end
soundsc(mus, 11025)