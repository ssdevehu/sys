
[melo, fs] = audioread("labs/lab2/melodia.wav");
figure
tiledlayout(3,1);
nexttile;

plot(melo), title("melodia.wav"), xlabel("n"), ylabel("y");
soundsc(melo, fs);


%Primera nota
% nexttile;
% plot(3200:3300, melo(3200:3300));

nexttile;
plot(11500:11700, melo(11500:11700)), title("2º Nota"), xlabel("n");


% nexttile;
% plot(11500:11600, melo(11500:11600));


nexttile;
plot(17400:17600, melo(17400:17600)), title("4º Nota"), xlabel("n");

% nexttile;
% plot(26800:26900, melo(26800:26900));