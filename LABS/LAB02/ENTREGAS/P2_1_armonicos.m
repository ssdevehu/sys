

% 
% s1(t) = 0.5 cos(2π 220 t) + 0.5 cos(2π 440 t)
% s2(t) = 0.5 cos(2π 220 t) + 0.5 cos(2π 440 t + π/2)

t = 0:1/8000:1;
s1a = 0.5 * cos(2*pi*220*t);
s1b = 0.5 * cos(2 *pi*440*t);

% 
% figure;
% tiledlayout(2,2);
% 
% nexttile;
% plot(s1a+s1b);
% nexttile;
% plot(s1a+s1b);
% nexttile;
% 
s2a = 0.5 * cos(2*pi*220*t);
s2b = 0.5 * cos(2*pi*440*t + pi/2);
% nexttile;
% plot(s2a+s2b);
% nexttile;
% plot(s2a+s2b);

soundsc(s1a+s1b)
figure, plot(s1a+s1b)
pause

soundsc(s2a+s2b)

figure
plot(s2a+s2b)